# scholae(1) -- Presenting markdown files in a browser for teaching

## SYNOPSIS

    scholae [options] [<file.md>]

## DESCRIPTION

Starts a local server to render "markdown" files within your browser:

* Runs on `localhost:4000`
* Supports [Markdown Syntax][]  
  with [GFM][].
* Markdown rendering based on [markdown-it][].   
  Enabled Plugins for: Emojis, Task lists, Footnotes, [KaTeX][]
* Includes [markedpp][] as preprocessor.
* Supports syntax highlighting using [highlight.js][]
* For supported markdown syntax see the Cheatsheet.
* Automatic update in browser after saving edited file. (Tested on Linux, MacOS, Windows)

## OPTIONS

* `-h`, `--help`:
  Display this help and exit.

* `--version`:
  Output version information and exit.

* `-p`, `--port` `<number=4000>`:
  Change the default port of the server.
  Default is 4000.

* `-b`, `--browser` `<exe>`:
  Use a different browser executable.

## EXAMPLES

Start the server:

    scholae

Open a document in the default browser:

    scholae README.md

Open the configuration page:

    scholae /config

Open my home directory:

    scholae /

Open the cheatsheet:

    scholae /cheatsheet

Start with a different browser on port 2000:

    scholae -p 2000 -b firefox

## INSTALLATION

    npm i -g scholae-dicimus

## COPYRIGHT

Copyright (c) 2019- commenthol - MIT License

## REPORTING BUGS

scholae repository <https://gitlab.com/commenthol/scholae/issues>

[KaTeX]: https://katex.org/
[GFM]: https://help.github.com/articles/github-flavored-markdown
[highlight.js]: http://highlightjs.org
[markdown-it]: https://github.com/markdown-it/markdown-it
[markedpp]: https://github.com/commenthol/markedpp
[Markdown Syntax]: http://daringfireball.net/projects/markdown/syntax
