# scholae-dicimus

> Presenting markdown files in a browser for teaching

[![NPM version](https://badge.fury.io/js/scholae-dicimus.svg)](https://www.npmjs.com/package/scholae-dicimus)

Forked of from [md-fileserver][] with focus on displaying markdown files for presentations.

Starts a local server to render "markdown" files within your browser:

* Runs on `localhost:4000`
* Supports [Markdown Syntax][] with [GFM][].
* Markdown rendering based on [markdown-it][].   
  Enabled Plugins for:
  * Emojis
  * Task lists
  * Footnotes
  * [KaTeX][]
* Includes [markedpp][] as preprocessor.
* Supports syntax highlighting using [highlight.js][]
* Include speaker notes with `<x-note>`
* Add Whiteboard for drawings with `<x-whiteboard>`
* Open Presentation Mode with <kbd>F5</kbd> to show presentation on external display with synchronous scrolling and drawing.
* Switch to speaker mode by double-click into browser window. The other window(s) will switch to presentation mode which mutes `<x-note>` and `<remark>` for listeners.
* For supported markdown Syntax and additional tags see [Cheatsheet][].
* Automatic update in browser after saving edited file. (Tested on Linux, MacOS, Windows)

## Table of Contents

<!-- !toc (minlevel=2 omit="Table of Contents") -->

* [Installation](#installation)
* [Usage](#usage)
  * [Start the Local Server](#start-the-local-server)
  * [Open a markdown file](#open-a-markdown-file)
* [Configuration](#configuration)
* [Cheatsheet](#cheatsheet)
* [Personalisation](#personalisation)
* [Contribution](#contribution)
* [License](#license)
* [References](#references)

<!-- toc! -->

## Installation

```bash
npm install -g scholae-dicimus
```

## Usage

### browser oddities

For Chrome on Windows 10 you'll need to disable "Direct Manipulation Stylus".
Open Chrome and enter `chrome://flags/#direct-manipulation-stylus` to disable flag.
Otherwise Stylus actions interfere with scroll events which does not allow painting.

### Start the Local Server

In your terminal type:

```
scholae
```

__Note__: The local server can only be reached from your local computer on port 4000. Any remote access from other computers to your files is denied.

### Open a markdown file

Open <http://localhost:4000> in your browser and navigate to the markdown file.

Alternatively type in your terminal:

```
scholae <file>
```

This will open the default browser with the processed markdown file.

## Configuration

```
scholae /config
```

## Cheatsheet

```
scholae test/cheatsheet.md
```

[Cheatsheet][]

## Personalisation

In `config.js` you can change several settings to fit your needs. These include:

* **Browser**: Default browser of MacOS, Linux or Windows is used.
* **Markdown** options: Change the options how [markdown-it][] processes your markdown files.
* **Markdown PP** options: Change the options how [markedpp][] pre-processes your markdown files.

Install personalized version:

1.  Clone this repo
    ````
    git clone --depth 2 https://gitlab.com/commenthol/scholae-dicimus.git
    cd scholae-dicimus
    ````
2.  Make your changes in `./config.js`
3.  Install with `npm i -g` from same folder.  
    If there are issues with installing you'll need to uninstall first with `npm un -g`

## Contribution

If you contribute code to this project, you are implicitly allowing your code
to be distributed under the MIT license. You are also implicitly verifying that
all code is your original work.

## License

Copyright (c) 2019-present commenthol   
Software is released under [MIT License][].

Bundled fonts from Font Awesome Free 5.x.x by @fontawesome - https://fontawesome.com
License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)

Bundled fonts from KaTex by Khan Academy - https://github.com/Khan/KaTeX
License - https://github.com/Khan/KaTeX/LICENSE.txt (Fonts: SIL OFL 1.1, Code: MIT License)

## References

<!-- !ref -->

* [Cheatsheet][Cheatsheet]
* [GFM][GFM]
* [highlight.js][highlight.js]
* [KaTeX][]
* [Markdown Syntax][Markdown Syntax]
* [markedpp][markedpp]
* [MIT License][MIT License]

<!-- ref! -->

[KaTeX]: https://katex.org/
[Cheatsheet]: test/cheatsheet.md
[GFM]: https://help.github.com/articles/github-flavored-markdown
[highlight.js]: http://highlightjs.org
[markdown-it]: https://github.com/markdown-it/markdown-it
[markedpp]: https://github.com/commenthol/markedpp
[md-fileserver]: https://github.com/commenthol/md-fileserver
[Markdown Syntax]: http://daringfireball.net/projects/markdown/syntax
[MIT License]: ./LICENSE
