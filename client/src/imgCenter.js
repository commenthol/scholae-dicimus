import { jq, jqall } from 'whiteboard-painter'

/**
 * open all external links in other window/tab
 */
export const imgCenter = () => {
  jqall('img').each(el => {
    const $el = jq(el)
    if (!$el.hasAttr('nocenter') && !/\bnocenter\b/.test($el.attr('alt'))) {
      const $center = jq().create('div').addClass('center')
      $el.after($center)
      $center.append($el)
    }
  })
}
