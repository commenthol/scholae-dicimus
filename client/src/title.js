import { jq } from 'whiteboard-painter'

/**
 * open all external links in other window/tab
 */
export const title = () => {
  const $title = jq('head title')
  const $txt = jq('body h0, body h1, body h2')
  if ($txt.node) $title.text($txt.text() + ' - ' + $title.text())
}
