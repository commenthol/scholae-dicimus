import { jq, jqall } from 'whiteboard-painter'

/**
 * open all external links in other window/tab
 */
export const aTargetBlank = () => {
  jqall('a[href]').each(el => {
    const $el = jq(el)
    const href = $el.attr('href')
    if (/^https?:\/\//.test(href)) {
      $el.attr('target', '_blank')
    }
  })
}
