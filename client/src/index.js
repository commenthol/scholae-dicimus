// default styles
import 'normalize.css'
import 'katex/dist/katex.min.css'
import '../css/screen.css'
import '../css/print.css'

import { xWhiteboards, xNotes } from 'whiteboard-painter'
import { Dispatcher } from './Dispatcher'
import { createConnection } from './reload'
import { aTargetBlank } from './aTargetBlank'
import { imgCenter } from './imgCenter'
import { title } from './title'
/**
 * wire whiteboard(s) with websocket
 */
const onMessage = new Dispatcher()
const { emitter } = onMessage
const conn = createConnection({ onMessage })
const onDraw = (payload) => conn.send(payload)
xWhiteboards({ emitter, onDraw })
xNotes({ emitter })
aTargetBlank()
imgCenter()
title()
