const CHANGE = 'change'

export class Dispatcher {
  constructor () {
    this.emitter = new EventTarget()
  }

  /**
   * @param {Object} payload
   */
  dispatch (payload) {
    const ev = new CustomEvent(CHANGE, { detail: payload })
    this.emitter.dispatchEvent(ev)
  }

  /**
   * @typedef {Object} Remover
   * @property {Function} remove - call to remove event listener
   */
  /**
   * @param {Function} fn - function to register
   * @return {Remover} event listener remover
   */
  addListener (fn) {
    const _fn = (ev) => fn(ev.detail)
    this.emitter.addEventListener(CHANGE, _fn)
    return {
      remove: () => this.emitter.removeEventListener(CHANGE, _fn)
    }
  }
}
